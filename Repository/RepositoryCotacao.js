const axios     = require('axios');

module.exports = class RepositoryCotacao {

    static async SendConteudods(json) {
        try {
            let response = await axios.post('https://api.conteudods.com.br/cotacao/create' , {
                json: json
            },{
                headers: { 
                    Authorization: 'Basic 3iyBk4hZjjU0t0ok8HHv7SZXJ7k5fM9d' 
                },
            })
            if(response.data.status) {
                return true
            }
            return false
        }
        catch(err) {
            return err
        }  
    }    

    static async SendConteudodsBovespa(arrayBovespa , arrayMaior , arrayMenor) {
        try {
            let response = await axios.post('https://api.conteudods.com.br/cotacao/bovespa' , {
                bovespa: arrayBovespa,
                arrayMaiores: arrayMaior ,
                arrayBaixas: arrayMenor
            },{
                headers: { 
                    Authorization: 'Basic 3iyBk4hZjjU0t0ok8HHv7SZXJ7k5fM9d' 
                },
            })
           
            if(response.data.status) {
                return true
            }
            return false
        }
        catch(err) {
            return err
        }
    }
}