const Helpers         = require('../helpers.js');
const { format }      = require('date-fns')

const RepositoryCotacao = require('../Repository/RepositoryCotacao.js');

module.exports = class CotacaoService  {
  
    static async SendCotacao(driver , By , until) {
        try {

            await driver.get('https://economia.uol.com.br/cotacoes/');

            let result = await driver.wait(until.elementsLocated(By.css('div.info')));
            let json = []

            for(let e of result) {
                
                json.push({
                    name: await e.findElement(By.css('span.name')).getText(),
                    porcentagem: await e.findElement(By.css('span.data')).getText(),
                    value: await e.findElement(By.css('div.numbers span.value')).getText()
                })
            }
            
            let jsonNew = []

            for(let i = 0; i < 4; i ++) {
                
                jsonNew.push({
                    name: json[i].name,
                    porcentagem: json[i].porcentagem,
                    value: json[i].value
                })
            }

            

            let arrayBovespa = []
            let Bovesp = await driver.wait(until.elementsLocated(By.css('section.stock div.info')));
            
            for(let e of Bovesp) {
                arrayBovespa.push(
                    {
                        name: await e.findElement(By.css('span.name')).getText(),
                        porcentagem: await e.findElement(By.css('span.data')).getText(),
                        value: await e.findElement(By.css('div.numbers span.value')).getText()
                    }
                )
            }

            
            await driver.get('https://economia.uol.com.br/cotacoes/bolsas/bvsp-bovespa/');
            
            let tabelaBovespa = await driver.wait(until.elementsLocated(By.css('tr.dadosRankings td a')))
            let valueTable    = []
            let maiores_altas = []
            let maiores_baixa = []

            for(let e of tabelaBovespa) {
                valueTable.push(await e.getText())
            }

            for(let i = 0; i < 15 ; i ++) {
                maiores_altas.push(valueTable[i])
            }

            for(let i = 15; i < 30 ; i ++) {
                maiores_baixa.push(valueTable[i])
            }

            let cotacao = await RepositoryCotacao.SendConteudods(JSON.stringify(jsonNew))

            let bovespa = await RepositoryCotacao.SendConteudodsBovespa(arrayBovespa , maiores_altas , maiores_baixa)
            
            if(cotacao == true && bovespa == true) {
                return true;
            }
            
        }
        catch(err) {
            console.log(err)
            return false
        }
    }
}