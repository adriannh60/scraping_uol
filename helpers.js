module.exports = class Helpers {
    async formatLocality(locality){
        return await locality.slice(42,56)
    }
    async formatLocalityLotomania(locality){
        return await locality.slice(45,59)
    }
    async formatCodeConcurso(code){
        return await code.slice(9,13)
    }
    
    async formatDateConcurso(date){
        return await date.slice(15,25)
    }
    
    async formatDateProximoConcurso(date){
        return await date.slice(41,51)
    }
    
    async verifyAcumuladoSena(premiacao){
        return await premiacao[0].includes('Não houve ganhadores')
    }
    
    async verifyAcumuladoQuina(premiacao){
        return await premiacao[1].includes('Não houve ganhadores')
    }

    async verifyAcumuladoQuadra(premiacao){
        return await premiacao[2].includes('Não houve ganhadores')
    }
    async GetPremiacaoSena(premiacao){
        const pieces = premiacao[0].split(' ');
        return {
            pessoas: this.OnlyNumber(pieces[1]),
            value_premio: pieces[pieces.length - 1]
        };
    }
    
    async GetPremiacaoQuina(premiacao){
        const pieces = premiacao[1].split(' ');
        return {
            pessoas: this.OnlyNumber(pieces[1]),
            value_premio: pieces[pieces.length - 1]
        };
    }
    
    async GetPremiacaoQuadra(premiacao){
        const pieces = premiacao[2].split(' ');
        return {
            pessoas: this.OnlyNumber(pieces[1]),
            value_premio: pieces[pieces.length - 1]
        };
    }
    
    OnlyNumber(string){
        let numsStr = string.replace(/[^0-9]/g,'');
        return parseInt(numsStr);
    }

    async sliceString(value , pos){
        
        const  pieces = value.split(' ');
        
        return await pieces[pos]
    }

    async removeCaracter(value){
        return await value.normalize('NFD').replace(/[.!'@,><|://\\;&*()_+=]/g, "")
    }

    async validateGanhadoresLotofacil(pessoa , vpremio , premioVerify){

        if(premioVerify.includes('Não houve acertador')){
            return {
                pessoas: 0,
                vpremios: '0.00'
            }
        }
        if(pessoa == 0){
            return {
                pessoas: '0',
                vpremios: '0.00'
            }
        }
        return { 
            pessoas: pessoa,
            vpremios: vpremio
        }
    }
}