const exec         = require('child_process').exec;
const webdriver    = require('selenium-webdriver');
const chrome       = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');
const { format }   = require('date-fns');

const CotacaoService = require('./Services/CotacaoService.js')

var { By, until } = webdriver;

(async function constructor() {
    try{
        chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());

        var driver = new webdriver.Builder()
            .withCapabilities(webdriver.Capabilities.chrome())
            .forBrowser('chrome')
            .setChromeOptions(
                new chrome.Options()
                    .headless()
                    .addArguments('disable-gpu', 'no-sandbox', 'disable-dev-shm-usage')
            )
            .build();
            
            if(await CotacaoService.SendCotacao(driver , By , until)) {
                console.log('salve')
            }
            else {
                console.log('error')
            }
        
        }
        catch(err){
            console.log(err)
        }
        finally {
            await driver.quit();  // Close the console app that was used to kick off the chrome window  
        }
})()
.then(() => {
    console.log('Finalizou o scrapping');
})
.catch((err) => {
    console.log(err);
})
.finally(() => {
    process.exit(1);
});
  

const isRunning = (query) => {
    return new Promise((resolve, reject) => {
        let platform = process.platform;
        let cmd = '';
        switch (platform) {
            case 'win32' : cmd = `tasklist`; break;
            case 'darwin' : cmd = `ps -ax | grep ${query}`; break;
            case 'linux' : cmd = `ps -A`; break;
            default: break;
        }
        exec(cmd, (err, stdout, stderr) => {
            resolve(stdout.toLowerCase().indexOf(query.toLowerCase()) > -1);
        });
    })
}
